package lockcheck_test

import (
	"testing"

	"gitlab.com/I3VNetDisk/i3vnd/analysis/lockcheck"
	"golang.org/x/tools/go/analysis/analysistest"
)

func Test(t *testing.T) {
	analysistest.Run(t, analysistest.TestData(), lockcheck.Analyzer, "a")
}
