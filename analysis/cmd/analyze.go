package main

import (
	"gitlab.com/I3VNetDisk/i3vnd/analysis/jsontag"
	"gitlab.com/I3VNetDisk/i3vnd/analysis/lockcheck"
	"gitlab.com/I3VNetDisk/i3vnd/analysis/responsewritercheck"
	"golang.org/x/tools/go/analysis/multichecker"
)

func main() {
	multichecker.Main(
		lockcheck.Analyzer,
		responsewritercheck.Analyzer,
		jsontag.Analyzer,
	)
}
