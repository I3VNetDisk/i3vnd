package wallet

import (
	"gitlab.com/I3VNetDisk/i3vnd/modules"
)

// Alerts implements the Alerter interface for the wallet.
func (w *Wallet) Alerts() []modules.Alert {
	return []modules.Alert{}
}
