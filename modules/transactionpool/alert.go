package transactionpool

import "gitlab.com/I3VNetDisk/i3vnd/modules"

// Alerts implements the modules.Alerter interface for the transactionpool.
func (tpool *TransactionPool) Alerts() []modules.Alert {
	return []modules.Alert{}
}
