package consensus

import (
	"gitlab.com/I3VNetDisk/i3vnd/modules"
)

// Alerts implements the Alerter interface for the consensusset.
func (c *ConsensusSet) Alerts() []modules.Alert {
	return []modules.Alert{}
}
