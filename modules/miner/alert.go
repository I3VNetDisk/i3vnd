package miner

import "gitlab.com/I3VNetDisk/i3vnd/modules"

// Alerts implements the modules.Alerter interface for the miner.
func (m *Miner) Alerts() []modules.Alert {
	return []modules.Alert{}
}
